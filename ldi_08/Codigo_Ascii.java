/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ldi_08;

/**
 *
 * @author Eli
 */
public class Codigo_Ascii {
    private char valor;
    private int valorascii;

    public Codigo_Ascii(char valor, int valorascii) {
        this.valor = valor;
        this.valorascii = valorascii;
    }

    public int getValorascii() {
        return valorascii;
    }

    public void setValorascii(int valorascii) {
        this.valorascii = valorascii;
    }

    public char getValor() {
        return valor;
    }

    public void setValor(char valor) {
        this.valor = valor;
    }
    
    
    
}
