/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ldi_08;

import java.util.Arrays;

/**
 *
 * @author Eli
 */
public class Ldi_08 {

    Codigo_Ascii arreglo[];

    public Ldi_08() {
        arreglo= new Codigo_Ascii[51];
        arreglo[0]= new Codigo_Ascii('A', 65);
        arreglo[1]= new Codigo_Ascii('B', 66);
        arreglo[2]= new Codigo_Ascii('C', 67);
        arreglo[3]= new Codigo_Ascii('D', 68);
            arreglo[4]= new Codigo_Ascii('E', 69);
        arreglo[5]= new Codigo_Ascii('F', 70);
        arreglo[6]= new Codigo_Ascii('G', 71);
        arreglo[7]= new Codigo_Ascii('H', 72);
            arreglo[8]= new Codigo_Ascii('I', 73);
        arreglo[9]= new Codigo_Ascii('J', 74);
        arreglo[10]= new Codigo_Ascii('K', 75);
        arreglo[11]= new Codigo_Ascii('L', 76);
            arreglo[12]= new Codigo_Ascii('M', 77);
        arreglo[13]= new Codigo_Ascii('N', 78);
        arreglo[14]= new Codigo_Ascii('O', 79);
        arreglo[15]= new Codigo_Ascii('P', 80);
            arreglo[16]= new Codigo_Ascii('Q', 81);
        arreglo[17]= new Codigo_Ascii('R', 82);
        arreglo[18]= new Codigo_Ascii('S', 83);
        arreglo[18]= new Codigo_Ascii('T', 84);
            arreglo[19]= new Codigo_Ascii('U', 85);
        arreglo[20]= new Codigo_Ascii('V', 86);
        arreglo[21]= new Codigo_Ascii('W', 87);
        arreglo[22]= new Codigo_Ascii('X', 88);
            arreglo[23]= new Codigo_Ascii('Y', 89);
        arreglo[24]= new Codigo_Ascii('Z', 90);
        
        //Minusculas
        arreglo[25]= new Codigo_Ascii('a', 97);
        arreglo[26]= new Codigo_Ascii('b', 98);
        arreglo[27]= new Codigo_Ascii('c', 99);
        arreglo[28]= new Codigo_Ascii('d', 100);
        arreglo[29]= new Codigo_Ascii('e', 101);
        arreglo[30]= new Codigo_Ascii('f', 102);
        
        arreglo[31]= new Codigo_Ascii('g', 103);
        arreglo[32]= new Codigo_Ascii('h', 104);
        arreglo[33]= new Codigo_Ascii('i', 105);
        arreglo[34]= new Codigo_Ascii('j', 106);
        
        arreglo[35]= new Codigo_Ascii('k', 107);
        arreglo[36]= new Codigo_Ascii('l', 108);
        arreglo[37]= new Codigo_Ascii('m', 109);
        arreglo[38]= new Codigo_Ascii('n', 110);
        arreglo[39]= new Codigo_Ascii('o', 111);
        arreglo[40]= new Codigo_Ascii('p', 112);
        
        arreglo[41]= new Codigo_Ascii('q', 113);
        arreglo[42]= new Codigo_Ascii('r', 114);
        arreglo[43]= new Codigo_Ascii('s', 115);
        arreglo[44]= new Codigo_Ascii('t', 116);
        arreglo[45]= new Codigo_Ascii('u', 117);
        arreglo[46]= new Codigo_Ascii('v', 118);
        arreglo[47]= new Codigo_Ascii('w', 119);
        arreglo[48]= new Codigo_Ascii('x', 120);
        arreglo[49]= new Codigo_Ascii('y', 121);
        arreglo[50]= new Codigo_Ascii('z', 122);
        
        
        
        
    }

    public Codigo_Ascii[] getArreglo() {
        return arreglo;
    }
    
    
    public String valorASCII(String cadena){
        char [] arregloChar=cadena.toCharArray();
    String resultado="";
        
        for(int i=0;i<arregloChar.length;i++){
            
            for(int x=0;x<arreglo.length;x++){
                
                if(arregloChar[i]==arreglo[x].getValor()){
                    resultado+= " "+ConvierteDecaBinario(arreglo[x].getValorascii());
                            
                }
            }
        }
        
                
        
        return resultado;
    }
    
    public String ConvierteDecaBinario(int numDecimal){
      return Integer.toBinaryString(numDecimal);
  }
    
    
    
    
    public static void main(String[] args) {
        Ldi_08 l8=new Ldi_08();
        //System.out.println(""+l8.arreglo[0].getValorascii());
        System.out.println( "Valor ASCII "+l8.valorASCII("Eli"));
    
}
}